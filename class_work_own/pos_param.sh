#!/bin/bash

total=0
for num in $@
do
	total=`expr $total + $num`
done

echo "$total"
