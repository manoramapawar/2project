#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
int main(int argc,char *argv[])
{
	int iRet;
	if(argc != 3)
	{
		printf("syntax:%s<target_filepath><link_path>\n",argv[0]);
		exit(1);
	}
	iRet=symlink(argv[1],argv[2]);
	if(iRet<0)
	{
		perror("link()failed");
		exit(1);
	}
	return 0;
}
